from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import ListView, DetailView

from articles.forms import ArticleForm, CommentForm
from articles.models import Article
from members.models import Member


@login_required(login_url='/')
def submit_article_page(request):
    form = ArticleForm(request.POST or None, use_required_attribute=False)
    if form.is_valid():
        article = form.save(commit=False)
        article.author = request.user.member
        article.save()

        return redirect('articles:list')

    context = {
        'form': form,
    }
    return render(request, 'articles/submit.html', context)


@login_required(login_url='/')
def list_article_page(request):
    articles = Article.objects.filter(author=request.user.member)
    context = {
        'articles': articles,
    }
    return render(request, 'articles/list.html', context)


@login_required(login_url='/')
def delete_article_page(request, article_id):
    article = Article.objects.get(id=article_id)
    article.delete()
    return redirect('articles:list')


@login_required(login_url='/')
def update_article_page(request, article_id):
    # manual handling invalid article
    # try:
    #     article = Article.objects.get(id=article_id)
    # except Article.DoesNotExist:
    #     return HttpResponse('data gak ketemu')

    # get article or throw 404 if invalid id.
    article = get_object_or_404(Article, id=article_id)

    form = ArticleForm(request.POST or None, use_required_attribute=False, instance=article)
    if form.is_valid():
        form.save()
        return redirect('articles:list')

    context = {
        'form': form,
    }
    return render(request, 'articles/submit.html', context)


@login_required(login_url='/')
def publish_article_page(request, article_id):
    article = get_object_or_404(Article, id=article_id)
    if article.is_published:
        article.is_published = False
        article.published_at = None
    else:
        article.is_published = True
    article.save()
    return redirect('articles:list')


@login_required(login_url='/')
def submit_comment_page(request, article_id):
    form = CommentForm(request.POST or None)
    url = reverse('articles:detail', args=[article_id])
    if request.method == 'POST':
        if form.is_valid():
            article = get_object_or_404(Article, id=article_id)

            comment = form.save(commit=False)
            comment.article = article
            comment.author = request.user.member
            comment.save()

            return redirect(url)
    return redirect(url)


def statistic_page(request):
    """
    tampilkan secara berurut:
    - article paling banyak di komentari [v]
    - member paling banyak menulis komentar [v]
    - member paling banyak menulis article [v]
    """
    most_writer = (Member.objects.filter(article__is_published=True)
                                 .annotate(total_article=Count('article'))
                                 .order_by('-total_article')[:3])
    most_commenter = (Member.objects.annotate(total_comment=Count('comment'))
                                    .order_by('-total_comment')[:3])
    most_commented_articles = (Article.objects.annotate(total_comment=Count('comment'))
                                              .order_by('-total_comment')[:3])
    total_article = Article.objects.filter(is_published=True).count()

    context = {
        'most_writer': most_writer,
        'most_commenter': most_commenter,
        'most_commented_articles': most_commented_articles,
        'total_article': total_article,
    }
    return render(request, 'articles/statistics.html', context)


class LibraryPage(ListView):
    model = Article
    template_name = 'articles/library.html'

    def get_queryset(self):
        return Article.objects.filter(is_published=True)

    def get_context_data(self, *, object_list=None, **kwargs):
        kwargs['articles'] = self.get_queryset()
        return kwargs


class DetailArticlePage(DetailView):
    model = Article
    template_name = 'articles/detail.html'
    pk_url_kwarg = 'article_id'

    def get_context_data(self, **kwargs):
        kwargs['article'] = self.get_object()
        kwargs['comment_form'] = CommentForm()
        return kwargs
