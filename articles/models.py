from django.db import models
from django.utils import timezone
from django.utils.text import slugify

from helpers.models import BaseModel
from members.models import Member


class Category(BaseModel):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Categories'


class Article(BaseModel):
    title = models.CharField(max_length=120)
    content = models.TextField()
    slug = models.SlugField()
    published_at = models.DateTimeField(null=True, blank=True)
    is_published = models.BooleanField(default=False)

    author = models.ForeignKey(Member, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        # Auto generate slug ketika save ke database.
        self.slug = slugify(self.title)

        # Auto isi published_at saat save dengan kondisi is_published
        # bernilai True.
        if self.is_published:
            self.published_at = timezone.now()

        # Save perubahan ke database.
        super(Article, self).save(*args, **kwargs)


class Comment(BaseModel):
    content = models.TextField()

    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    author = models.ForeignKey(Member, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.article.title}: {self.content}'


