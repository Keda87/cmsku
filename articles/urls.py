from django.urls import path

from articles.views import (
    submit_article_page,
    list_article_page,
    delete_article_page,
    update_article_page,
    publish_article_page,
    LibraryPage, DetailArticlePage, submit_comment_page, statistic_page
)

app_name = 'articles'

urlpatterns = [
    path('submit/', submit_article_page, name='submit'),
    path('list/', list_article_page, name='list'),
    path('libraries/', LibraryPage.as_view(), name='library'),
    path('stats/', statistic_page, name='stats'),
    path('detail/<int:article_id>', DetailArticlePage.as_view(), name='detail'),
    path('delete/<int:article_id>', delete_article_page, name='delete'),
    path('update/<int:article_id>', update_article_page, name='update'),
    path('publish/<int:article_id>', publish_article_page, name='publish'),
    path('comment/<int:article_id>', submit_comment_page, name='comment')
]
