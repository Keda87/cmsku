from django.contrib import admin

from articles.models import Category, Article, Comment


class ArticleAdminPage(admin.ModelAdmin):
    list_display = ['title', 'is_published', 'published_at', 'created_at']
    list_filter = ['is_published']
    search_fields = ['title']


class CategoryAdminPage(admin.ModelAdmin):
    list_display = ['name', 'created_at']


admin.site.register(Category, CategoryAdminPage)
admin.site.register(Article, ArticleAdminPage)
admin.site.register(Comment)
