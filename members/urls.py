from django.urls import path

from members.views import (
    sign_up_page,
    sign_in_page,
    sign_out_page,
    profile_page,
    deactivate_page,
)

app_name = 'members'

urlpatterns = [
    path('sign-out/', sign_out_page, name='sign-out'),
    path('sign-up/', sign_up_page, name='sign-up'),
    path('profile/', profile_page, name='profile'),
    path('deactivate/', deactivate_page, name='deactivate'),
    path('', sign_in_page, name='sign-in'),
]
