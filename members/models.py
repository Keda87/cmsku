import os
import shutil

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver

from helpers.models import BaseModel


def photo_path(instance, filename):
    return os.path.join('prof_pic', instance.user.username, filename)


class Member(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to=photo_path)

    def __str__(self):
        return self.user.username


@receiver(post_delete, sender=Member)
def signal_remove_photo(sender, **kwargs):
    member = kwargs.get('instance')
    user_profile_dir = os.path.dirname(member.photo.file.name)

    try:
        shutil.rmtree(user_profile_dir)
    except IOError:
        pass
