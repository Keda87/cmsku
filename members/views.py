from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.urls import reverse

from members.forms import SignUpForm, SignInForm
from members.models import Member


def sign_up_page(request):
    # FBV: function based views
    form = SignUpForm(request.POST or None, request.FILES or None, use_required_attribute=False)
    if request.method == 'POST':
        if form.is_valid():  # True/False
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            photo = form.cleaned_data.get('photo')

            user = User.objects.create_user(email, email, password)
            member = Member.objects.create(user=user, photo=photo)

            messages.success(request, f'Hi {member.user.username}, Sign Up Success')
            url = reverse('members:sign-up')
            return redirect(url)

    context = {
        'form_signup': form,
    }
    return render(request, 'members/signup.html', context)


def sign_in_page(request):
    if request.user.is_authenticated:
        return redirect('articles:list')

    form = SignInForm(request.POST or None, use_required_attribute=False)

    if request.method == 'POST':
        if form.is_valid():
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')

            # login / authenticate
            user = authenticate(username=email, password=password)
            if user is not None:
                login(request, user)
                messages.success(request, f'{user.username} is authenticated', extra_tags='is-success')
                return redirect('articles:list')

            messages.error(request, 'Mismatch email and password', extra_tags='is-danger')
            return redirect('members:sign-in')

    context = {
        'form': form
    }
    return render(request, 'members/signin.html', context)


def sign_out_page(request):
    logout(request)
    return redirect('members:sign-in')


@login_required(login_url='/')
def profile_page(request):
    context = {
        'member': request.user.member
    }
    return render(request, 'members/profile.html', context)


@login_required(login_url='/')
def deactivate_page(request):
    user = request.user
    user.delete()

    logout(request)

    return redirect('members:sign-up')
