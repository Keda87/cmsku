from django.contrib import admin

from members.models import Member


class MemberAdminPage(admin.ModelAdmin):
    list_display = ['get_username', 'get_email', 'created_at']
    search_fields = ['user__username']

    def get_username(self, obj):
        return obj.user.username
    get_username.short_description = 'Username'

    def get_email(self, obj):
        return obj.user.email
    get_email.short_description = 'Email'


admin.site.register(Member, MemberAdminPage)
